# Wordpress

Instalação do Wordpress em um cluster Kubernetes.

## Pré-requisitos

Para instalar o projeto você precisará criar o namespace `projetocompass` e ter [instalado o MySQL](https://gitlab.com/JadsonBraz/final-devops/-/blob/main/docs/install_mysql.md).

O projeto utiliza um Ingress Controller. Certifique-se de ter instalado em sua máquina.

## Instalando

Após preparar o ambiente, baixe o projeto e execute os comandos abaixo:

        kubectl apply -f wordpress-deployment.yaml
        kubectl apply -f ingress.yaml

Para ver se está tudo ok execute o comando

        kubectl get all -n projetocompass

Para abrir o site no seu navegador, primeiramente edite o seu arquivo hosts colocando o IP da máquina e o URL `projetofinal.compass.uol`. Salve o arquivo e acesse a URL no seu navegador.
